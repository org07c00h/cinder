//
//  Event.hpp
//  cinder
//
//  Created by Руфат Аминов on 07/04/16.
//  Copyright © 2016 ginko.moe. All rights reserved.
//

#pragma once
namespace Event
{
    enum Type {
        MOUSE_CLICK,
        ENEMY_DIED,
        PLAYER_DIED
    };
    
    struct mouseData {
        int x;
        int y;
        int button;
    };
    
    struct keyboardData
    {
        int keyCode;
        bool isUP;
    };
    
    union Data {
        mouseData mouse;
        keyboardData keyboard;
    };
    
}