//
//  ResourceManager.hpp
//  cinder
//
//  Created by Руфат Аминов on 03/04/16.
//  Copyright © 2016 ginko.moe. All rights reserved.
//
#pragma once
#include <map>
#include <string>

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

class ResourceManager
{
public:
    void loadTexture(const std::string &fileName, const std::string &name);
    const sf::Texture& getTexture(const std::string& name);
    
    void loadImage(const std::string &fileName, const std::string &name);
    const sf::Image& getImage(const std::string& name);
    
    void loadFont(const std::string &fileName, const std::string &name);
    const sf::Font& getFont(const std::string& name);
    
    void loadMusic(const std::string &fileName, const std::string &name);
    const sf::Music& getMusic(const std::string &name);
    
    static ResourceManager& getInstance();
private:
    ResourceManager();
    ~ResourceManager();
    
    std::map<std::string, sf::Texture> mTextures;
    std::map<std::string, sf::Image> mImages;
    std::map<std::string, sf::Font> mFonts;
    std::map<std::string, sf::Music> mMusics;
    
};

#define theResourceManager ResourceManager::getInstance()