//
//  Actor.hpp
//  cinder
//
//  Created by Руфат Аминов on 04/04/16.
//  Copyright © 2016 ginko.moe. All rights reserved.
//

#pragma once
#include <SFML/Graphics.hpp>

#include "Event.hpp"
class Actor
{
public:
    virtual void update(float dt) = 0;
    virtual const sf::Drawable& draw() = 0;
    virtual void onNotify(const Event::Type& eventType, const Event::Data& data) = 0;
    virtual ~Actor() {}
};