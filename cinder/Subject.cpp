//
//  Subject.cpp
//  cinder
//
//  Created by Руфат Аминов on 06/04/16.
//  Copyright © 2016 ginko.moe. All rights reserved.
//

#include "Subject.hpp"

namespace Event {
    
    Subject& Subject::getInstance()
    {
        static Subject myInstance;
        
        return myInstance;
    }
    
    Subject::Subject()
    {
        
    }
    
    void Subject::subscribe(Event::Type eventType, Actor *actor)
    {
        mObservers.insert(std::pair<Event::Type, Actor*>(eventType, actor));
    }
    
    void Subject::notify(Event::Type eventType, const Event::Data& data)
    {
        auto range = mObservers.equal_range(eventType);
        
        for (auto it = range.first; it != range.second; it++)
        {
            it->second->onNotify(eventType, data);
        }
    }
    
    void Subject::unsubscribe(Event::Type eventType, Actor *actor)
    {
        auto range = mObservers.equal_range(eventType);
        for (auto it = range.first; it != range.second; it++)
        {
            if (it->second == actor)
            {
                mObservers.erase(it);
                break;
            }
        }
    }
}