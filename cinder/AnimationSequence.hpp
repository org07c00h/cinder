//
//  AnimationSequence.hpp
//  cinder
//
//  Created by Руфат Аминов on 05/04/16.
//  Copyright © 2016 ginko.moe. All rights reserved.
//
#pragma once
#include <map>
#include <vector>
#include <SFML/Graphics.hpp>

/// Class for animation
class AnimationSequence
{
public:
    
    AnimationSequence(float duration);
    
    void addState(const std::string& name);
    /**
     *  adds sprite to the state
     *
     *  @param state  state's name
     *  @param sprite sprite to add
     */
    void addSprite(const std::string& state, const sf::Sprite& sprite);
    sf::Sprite& getCurrentSprite();
    void update(float dt);
    void setState(const std::string& name);
    std::string getCurrentState();
    ~AnimationSequence();
    
private:
    std::map<std::string, std::vector<sf::Sprite> > mSequences;
    
    std::string mCurrentState;
    unsigned int mCurrent;
    
    float mTimeElapsed;
    float mDuration;
};