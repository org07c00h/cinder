//
//  Game.cpp
//  cinder
//
//  Created by Руфат Аминов on 03/04/16.
//  Copyright © 2016 ginko.moe. All rights reserved.
//

#include "Game.hpp"
#include <iostream>
Game::Game(unsigned int width, unsigned int height) :
mWindow(sf::VideoMode(width, height), "foobar")
{
    mHeight = height;
    mWidth = width;
    
    mTitle = "Cinder";
    
    mWindow.setTitle(mTitle);
    mWindow.setFramerateLimit(60);
    running = true;
    
    init();
    mActors = theActorsManager.getActors();
}

void Game::init()
{
    theResourceManager.loadImage("icon.png", "icon");
    sf::Image icon = theResourceManager.getImage("icon");
    mWindow.setIcon(icon.getSize().x, icon.getSize().y,
                    icon.getPixelsPtr());
    theResourceManager.loadTexture("cute_image.jpg", "cute");
    mSprites.push_back(sf::Sprite(theResourceManager.getTexture("cute")));
    
    theResourceManager.loadTexture("character_1.png", "character1");
    theResourceManager.loadTexture("character_2.png", "character2");
    theResourceManager.loadTexture("character_3.png", "character3");
    theResourceManager.loadTexture("character_4.png", "character4");
    theActorsManager.addActor(new Player());
}

void Game::run()
{
    sf::Clock clock;
    float lastTime = 0;
    float dt;
    float fps;
    
    while (mWindow.isOpen() && running)
    {
        sf::Event event;
        
        while (mWindow.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                mWindow.close();
            }
            
            if (event.type == sf::Event::KeyPressed &&
                event.key.code == sf::Keyboard::Escape)
            {
                mWindow.close();
            }
            
            if (event.type == sf::Event::MouseButtonPressed)
            {
                Event::Data data;
                data.mouse.x = 1;
                data.mouse.y = 2;
                data.mouse.button = 3;

                theSubject.notify(Event::Type::MOUSE_CLICK, data);
            }
        }
        
        mWindow.clear();
        
        render();
        
        mWindow.display();
        
        dt = clock.restart().asSeconds();
        
        lastTime += dt;
        if (lastTime > 0.2)
        {
            fps = 1.f / (dt);
            mWindow.setTitle(mTitle + " FPS:" + std::to_string(int(fps)));
            lastTime = 0;
        }
        
        theActorsManager.update(dt);
    }
}

void Game::render()
{
    for (auto it = mSprites.begin(); it != mSprites.end(); it++)
    {
        mWindow.draw(*it);
    }
    
    for (auto it = mActors.begin(); it != mActors.end(); it++)
    {
        mWindow.draw((*it)->draw());
    }
}