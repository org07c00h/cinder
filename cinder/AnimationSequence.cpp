//
//  AnimationSequence.cpp
//  cinder
//
//  Created by Руфат Аминов on 05/04/16.
//  Copyright © 2016 ginko.moe. All rights reserved.
//

#include "AnimationSequence.hpp"
#include <iostream>
AnimationSequence::AnimationSequence(float duration)
{

    mDuration = duration;
    mTimeElapsed = 0;
}

sf::Sprite& AnimationSequence::getCurrentSprite()
{
    if (mSequences[mCurrentState].empty())
    {
        // TODO: throw exception like no_sprites;
    }
    return mSequences[mCurrentState][mCurrent];
}

void AnimationSequence::update(float dt)
{
    mTimeElapsed += dt;
    if (mTimeElapsed > mDuration)
    {
        mTimeElapsed = 0;
        mCurrent = (mCurrent + 1) % mSequences[mCurrentState].size();
    }
}

void AnimationSequence::addState(const std::string &name)
{

}

void AnimationSequence::addSprite(const std::string &state, const sf::Sprite &sprite)
{
    mSequences[state].push_back(sprite);
}

void AnimationSequence::setState(const std::string &name)
{
    if (!mCurrentState.empty())
    {
        mSequences[name][0].setPosition(mSequences[mCurrentState][mCurrent].getPosition());
    }
    mCurrentState = name;
    mTimeElapsed = 0;

    mCurrent = 0;
}

std::string AnimationSequence::getCurrentState()
{
    return mCurrentState;
}

AnimationSequence::~AnimationSequence()
{
    
}