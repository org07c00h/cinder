//
//  Player.hpp
//  cinder
//
//  Created by Руфат Аминов on 04/04/16.
//  Copyright © 2016 ginko.moe. All rights reserved.
//
#pragma once
#include <vector>
#include <SFML/Graphics.hpp>

#include "Actor.hpp"
#include "ResourceManager.hpp"
#include "AnimationSequence.hpp"

#include "Event.hpp"
#include "Subject.hpp"

class Player : public Actor
{
public:
    Player();
    void update(float dt);
    const sf::Drawable& draw();
    void onNotify(const Event::Type& eventType, const Event::Data& data);
    ~Player();
    
private:
    std::vector<sf::Sprite> mSprites;
    sf::Vector2f mPosition;
    float mAnimationDuration;
    float mTimeElapsed;
    unsigned int mCurrentSprite;
    AnimationSequence mAnimation;
};