//
//  ActorManager.cpp
//  cinder
//
//  Created by Руфат Аминов on 04/04/16.
//  Copyright © 2016 ginko.moe. All rights reserved.
//

#include "ActorsManager.hpp"

ActorsManager& ActorsManager::getInstance()
{
    static ActorsManager myInstance;
    return myInstance;
}

void ActorsManager::update(float dt)
{
    for (auto it = mActors.begin(); it != mActors.end(); it++)
    {
        (*it)->update(dt);
    }
}

void ActorsManager::addActor(Actor *actor)
{
    mActors.push_back(actor);
}

const std::list<Actor*>& ActorsManager::getActors()
{
    return mActors;
}

ActorsManager::ActorsManager()
{
    
}

ActorsManager::~ActorsManager()
{
    for (auto it = mActors.begin(); it != mActors.end(); it++)
    {
        delete *it;
    }
}