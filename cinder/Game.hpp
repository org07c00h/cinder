//
//  Game.hpp
//  cinder
//
//  Created by Руфат Аминов on 03/04/16.
//  Copyright © 2016 ginko.moe. All rights reserved.
//
#pragma once
#include <string>
#include <list>

#include <SFML/Graphics.hpp>

#include "ResourceManager.hpp"
#include "ActorsManager.hpp"
#include "Actor.hpp"
#include "Player.hpp"
#include "Event.hpp"
#include "Subject.hpp"


class Game {
public:
    Game(unsigned int width, unsigned int height);
    void run();
private:
    void init();
    void render();
    
    bool running;
    std::string mTitle;
    unsigned int mWidth;
    unsigned int mHeight;
    
    sf::RenderWindow mWindow;
    std::list<sf::Sprite> mSprites;
    std::list<Actor*> mActors;
};