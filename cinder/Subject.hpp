//
//  Subject.hpp
//  cinder
//
//  Created by Руфат Аминов on 06/04/16.
//  Copyright © 2016 ginko.moe. All rights reserved.
//

#pragma once
#include "Actor.hpp"
#include "Event.hpp"
#include <map>

namespace Event {
    class Subject
    {
    public:
        static Subject& getInstance();
        void notify(Event::Type eventType, const Event::Data& data);
        void subscribe(Event::Type eventType, Actor* actor);
        void unsubscribe(Event::Type eventType, Actor* actor);
    private:
        Subject();
        
        std::multimap<Event::Type, Actor*> mObservers;
    };
}

#define theSubject Event::Subject::getInstance()