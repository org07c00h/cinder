//
//  ActorManager.hpp
//  cinder
//
//  Created by Руфат Аминов on 04/04/16.
//  Copyright © 2016 ginko.moe. All rights reserved.
//

#pragma once
#include <list>
#include "Actor.hpp"

class ActorsManager
{
public:
    static ActorsManager& getInstance();
    void update(float dt);
    void addActor(Actor* actor);
    const std::list<Actor*>& getActors();
    
    ~ActorsManager();
private:
    ActorsManager();
    std::list<Actor*> mActors;
    
};

#define theActorsManager ActorsManager::getInstance()