//
//  Player.cpp
//  cinder
//
//  Created by Руфат Аминов on 04/04/16.
//  Copyright © 2016 ginko.moe. All rights reserved.
//

#include "Player.hpp"
#include <iostream>

Player::Player() :
mAnimation(0.5)
{

    
    mPosition = sf::Vector2f(0, 0);
    mAnimationDuration = 0.5;
    mTimeElapsed = 0;
    mCurrentSprite = 0;
    
    mAnimation.addState("down");
    mAnimation.addState("up");
    mAnimation.addSprite("down", sf::Sprite(theResourceManager.getTexture("character1")));
    mAnimation.addSprite("down", sf::Sprite(theResourceManager.getTexture("character2")));
    mAnimation.addSprite("up", sf::Sprite(theResourceManager.getTexture("character3")));
    mAnimation.addSprite("up", sf::Sprite(theResourceManager.getTexture("character4")));
    mAnimation.setState("up");
    
    theSubject.subscribe(Event::Type::MOUSE_CLICK, this);
}

void Player::update(float dt)
{
    mAnimation.update(dt);
    mPosition.x = mPosition.y += 20*dt;

    mAnimation.getCurrentSprite().setPosition(mPosition);
}

const sf::Drawable& Player::draw()
{
    return mAnimation.getCurrentSprite();
}

void Player::onNotify(const Event::Type &eventType, const Event::Data &data)
{
    if (eventType == Event::Type::MOUSE_CLICK)
    {
        if (mAnimation.getCurrentState() == "down")
        {
            mAnimation.setState("up");
        }
        else
        {
            mAnimation.setState("down");
        }
    }
}

Player::~Player()
{
    
}

