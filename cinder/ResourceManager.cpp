//
//  ResourceManager.cpp
//  cinder
//
//  Created by Руфат Аминов on 03/04/16.
//  Copyright © 2016 ginko.moe. All rights reserved.
//

#include "ResourceManager.hpp"
// Here is a small helper for you ! Have a look.
#include "ResourcePath.hpp"
#include <iostream>

ResourceManager::ResourceManager()
{
    
}

ResourceManager::~ResourceManager()
{
    
}

ResourceManager& ResourceManager::getInstance()
{
    static ResourceManager myInstance;
    
    return myInstance;
}

// load/get Texture
const sf::Texture& ResourceManager::getTexture(const std::string& name)
{
    return mTextures[name];
}

void ResourceManager::loadTexture(const std::string &fileName, const std::string &name)
{
    sf::Texture texture;
    if (!texture.loadFromFile(resourcePath() + fileName)) {
        exit(EXIT_FAILURE);
    }

    mTextures[name] = texture;
}

// load/get Image
void ResourceManager::loadImage(const std::string &fileName, const std::string &name)
{
    sf::Image image;
    if (!image.loadFromFile(resourcePath() + "icon.png")) {
        exit(EXIT_FAILURE);
    }
    
    mImages[name] = image;
}

const sf::Image& ResourceManager::getImage(const std::string &name)
{
    return mImages[name];
}

// load/get Font
void ResourceManager::loadFont(const std::string &fileName, const std::string &name)
{
    sf::Font font;
    if (!font.loadFromFile(resourcePath() + fileName))
    {
        exit(EXIT_FAILURE);
    }
    mFonts[name] = font;
}

const sf::Font& ResourceManager::getFont(const std::string &name)
{
    return mFonts[name];
}


// load/get Music
void ResourceManager::loadMusic(const std::string &fileName, const std::string &name)
{
    sf::Music music;
    if (!music.openFromFile(resourcePath() + fileName))
    {
        exit(EXIT_FAILURE);
    }
}

const sf::Music& ResourceManager::getMusic(const std::string &name)
{
    return mMusics[name];
}